<?php

namespace Drupal\superfaktura\Event;

use Drupal\commerce_order\Entity\Order;
use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the invoice post send event.
 */
class InvoicePostSendEvent extends Event {

  /**
   * Created order.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  protected $order;

  /**
   * Object of SF Client.
   *
   * @var \SFAPIclient
   */
  protected $data;

  /**
   * Object returned from SF Service.
   *
   * Can contain the error message if the request failed.
   *
   * @var \StdClass
   */
  protected $response;

  /**
   * InvoiceCreatedEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Order object.
   * @param \SFAPIclient $data
   *   Object of SF Client.
   * @param \StdClass $response
   *   Object returned from SF Service.
   */
  public function __construct(Order $order, \SFAPIclient $data, \StdClass $response) {
    $this->order = $order;
    $this->data = $data;
    $this->response = $response;
  }

  /**
   * Returns the order where the invoice generating started.
   *
   * @return \Drupal\commerce_order\Entity\Order
   *   Order.
   */
  public function getOrder(): Order {
    return $this->order;
  }

  /**
   * Returns the invoice data which was sent.
   *
   * @return \SFAPIclient
   *   Object of SF Client.
   */
  public function getData(): \SFAPIclient {
    return $this->data;
  }

  /**
   * Returns the object returned from SF Service.
   *
   * Can contain the error message if the request failed.
   *
   * @return \StdClass
   *   Object of SF Client.
   */
  public function getResponse(): \StdClass {
    return $this->response;
  }

}
