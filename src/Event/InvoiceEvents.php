<?php

namespace Drupal\superfaktura\Event;

/**
 * Defines events for the superfaktura module.
 */
final class InvoiceEvents {

  /**
   * Name of event before sending data to Superfaktura.
   *
   * The data sent in request can be altered.
   *
   * @Event
   *
   * @see \Drupal\superfaktura\Event\InvoicePreSendEvent
   */
  const INVOICE_PRE_SEND = 'superfaktura.invoice_pre_send';

  /**
   * Name of the event after sending data to Superfaktura.
   *
   * This event fires always after sending data. Even if the request
   * didn't succeed.
   *
   * @Event
   *
   * @see \Drupal\superfaktura\Event\InvoicePostSendEvent
   */
  const INVOICE_POST_SEND = 'superfaktura.invoice_post_send';

  /**
   * Name of the event fired after invoice is created.
   *
   * This fires only if the invoice was created on Superfaktura successfully.
   *
   * @Event
   *
   * @see \Drupal\superfaktura\Event\InvoiceCreatedEvent
   */
  const INVOICE_CREATED = 'superfaktura.invoice_created';

}
