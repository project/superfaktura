<?php

namespace Drupal\superfaktura\Event;

use Drupal\commerce_order\Entity\Order;
use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the invoice pre send event.
 */
class InvoicePreSendEvent extends Event {

  /**
   * Created order.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  protected $order;

  /**
   * Object of SF Client.
   *
   * @var \SFAPIclient
   */
  protected $data;

  /**
   * InvoiceCreatedEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Order object.
   * @param \SFAPIclient $data
   *   Object of SF Client.
   */
  public function __construct(Order $order, \SFAPIclient $data) {
    $this->order = $order;
    $this->data = $data;
  }

  /**
   * Returns the order where the invoice generating started.
   *
   * @return \Drupal\commerce_order\Entity\Order
   *   Order.
   */
  public function getOrder(): Order {
    return $this->order;
  }

  /**
   * Returns the invoice data which will be sent.
   *
   * @return \SFAPIclient
   *   Object of SF Client.
   */
  public function getData(): \SFAPIclient {
    return $this->data;
  }

  /**
   * Sets the invoice data which will be sent.
   *
   * @param \SFAPIclient $data
   *   Object of SF Client.
   */
  public function setData(\SFAPIclient $data): \SFAPIclient {
    $this->data = $data;
    return $data;
  }

}
