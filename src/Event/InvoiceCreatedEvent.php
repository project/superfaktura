<?php

namespace Drupal\superfaktura\Event;

use Drupal\commerce_order\Entity\Order;
use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the invoice created event.
 */
class InvoiceCreatedEvent extends Event {

  /**
   * Created order.
   *
   * @var \Drupal\commerce_order\Entity\Order
   */
  protected $order;

  /**
   * Invoice object returned from SF Service.
   *
   * @var \StdClass
   */
  protected $invoice;

  /**
   * InvoiceCreatedEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Order object.
   * @param \StdClass $invoice
   *   Invoice object.
   */
  public function __construct(Order $order, \StdClass $invoice) {
    $this->order = $order;
    $this->invoice = $invoice;
  }

  /**
   * Gets order.
   *
   * @return \Drupal\commerce_order\Entity\Order
   *   Order for which event is fired.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Gets invoice.
   *
   * @return \StdClass
   *   Invoice created for order.
   */
  public function getInvoice() {
    return $this->invoice;
  }

}
