<?php

namespace Drupal\superfaktura;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_price\Calculator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\superfaktura\Event\InvoiceCreatedEvent;
use Drupal\superfaktura\Event\InvoiceEvents;
use Drupal\superfaktura\Event\InvoicePostSendEvent;
use Drupal\superfaktura\Event\InvoicePreSendEvent;

/**
 * Implements logic for creating and sending invoices to Superfaktura.
 *
 * @package Drupa\superfaktura
 */
class InvoiceService {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * LoggerChannelInterface object.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Conversion array for converting from 2 to 3 lang code.
   *
   * @var array
   */
  protected $conversion;

  /**
   * Entity storage for payments.
   *
   * @var \Drupal\commerce_payment\Entity\Payment
   */
  protected $paymentEntityManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * InvoiceService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Superfaktura settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $loggerFactory, LanguageManagerInterface $languageManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->configFactory = $configFactory->get('superfaktura.settings');
    $this->logger = $loggerFactory->get('superfaktura');
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->conversion = json_decode(file_get_contents(__DIR__ . '/../resources/iso-conversion.json'), TRUE);
    $this->paymentEntityManager = $this->entityTypeManager->getStorage('commerce_payment');
  }

  /**
   * Convert from 2letter langcode to 3letter langcode.
   *
   * @param string $lang2
   *   Two-letter langcode to convert.
   *
   * @return string
   *   Converted 3 letter langcode.
   */
  public function convertLang2to3(string $lang2) {
    return $this->conversion[$lang2];
  }

  /**
   * Create Invoice using Superfaktura API.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Being thrown if invalid entity type is provided.
   */
  public function createInvoice(Order $order) {
    $order_discount = 0;
    $order_discount_label = NULL;

    $api = $this->getSfClient($this->languageManager->getCurrentLanguage()
      ->getId());

    $api->setClient($this->addClient($order));
    $api->setInvoice($this->addInvoice($order));

    // Define shipping tax amount and percentage,
    // shipping promotions amount and percentage default values
    // as shipping_data.
    $shipping_data = [
      'shipping_discount_label' => '',
      'shipping_discount_amount' => NULL,
      'shipping_discount_percentage' => NULL,
      'shipping_tax_amount' => '0',
      'shipping_tax_percentage' => '0',
    ];

    $order_adjustments = $order->getAdjustments();
    /** @var \Drupal\commerce_order\Adjustment $order_adjustment */
    foreach ($order_adjustments as $order_adjustment) {
      if ($order_adjustment->getType() == 'shipping' && !$order_adjustment->isIncluded()) {
        $shipping_adjustment = $order_adjustment;
      }
      elseif ($order_adjustment->getType() == 'tax') {
        // Get shipping tax amount and percentage from order adjustment.
        $shipping_data['shipping_tax_amount'] = $order_adjustment->getAmount()
          ->getNumber();
        $shipping_data['shipping_tax_percentage'] = $order_adjustment->getPercentage();
        $shipping_data['shipping_tax_included_in_price'] = $order_adjustment->isIncluded();

      }
      elseif ($order_adjustment->getType() == 'shipping_promotion' && !$order_adjustment->isIncluded()) {
        $shipping_data['shipping_discount_label'] = $order_adjustment->getLabel();
        $shipping_data['shipping_discount_amount'] = $order_adjustment->getAmount()
          ->getNumber();
        if (!is_null($order_adjustment->getPercentage())) {
          $shipping_data['shipping_discount_percentage'] = $order_adjustment->getPercentage();
        }
      }
      elseif ($order_adjustment->getType() == 'promotion' && !$order_adjustment->isIncluded()) {
        $api->setInvoice('discount_total', 0);
        $api->setInvoice('discount', 0);
        if (!is_null($order_adjustment->getPercentage())) {
          $order_discount = $order_adjustment->getPercentage() * 100;
          $order_discount_label = $order_adjustment->getLabel();
        }
        else {
          $api->setInvoice('discount_total', $order_adjustment->getAmount()
            ->getNumber());
        }
      }
    }

    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $item */
    foreach ($order->getItems() as $item) {
      $api->addItem($this->addOrderItem($item, $order_discount, $order_discount_label));
    }

    if (isset($shipping_adjustment)) {
      $api->addItem($this->addShippingItem($shipping_adjustment, $shipping_data));
    }

    $event_dispatcher = \Drupal::service('event_dispatcher');

    // Event before sending the request.
    // Other modules can alter the sent data.
    $preSendEvent = new InvoicePreSendEvent($order, $api);
    $event_dispatcher->dispatch(InvoiceEvents::INVOICE_PRE_SEND, $preSendEvent);

    $response = $api->save();

    // Event after sending the request.
    // Other modules can do things when the request failed or succeeded.
    $postSendEvent = new InvoicePostSendEvent($order, $api, $response);
    $event_dispatcher->dispatch(InvoiceEvents::INVOICE_POST_SEND, $postSendEvent);

    if ($response->error === 0) {
      $invoice_id = $response->data->Invoice->id;
      $this->logger->info('Invoice #@invoice was successfully created.', ['@invoice' => $invoice_id]);
      $order->set('superfaktura_invoice_id', $invoice_id);

      // Download the invoice PDF.
      $orderInvoiceFileFieldname = $this->configFactory->get('order_file_field');
      if (!empty($orderInvoiceFileFieldname) && $order->hasField($orderInvoiceFileFieldname)) {
        // Remove older invoice PDF references.
        $fieldValue = [];
        $file = $this->createInvoiceFile($api, $response);

        if ($file) {
          $fieldValue[] = [
            'target_id' => $file->id(),
          ];
          $order->set($orderInvoiceFileFieldname, $fieldValue);
        }
        else {
          $this->logger->error(
            "PDF for order @order (@invoice) wasn't saved.", [
              '@order' => $order->id(),
              '@invoice' => $invoice_id,
            ]
          );
        }

      }

      // Event after creating the invoice successfully.
      // Other modules can do things with the newly created invoice.
      $invoiceCreatedEvent = new InvoiceCreatedEvent($order, $response);
      $event_dispatcher->dispatch(InvoiceEvents::INVOICE_CREATED, $invoiceCreatedEvent);
      return 0;
    }
    else {
      switch ($response->error) {
        case 2:
          $this->logger->error('Data not sent by POST method. ' . ((isset($response->error_message)) ? print_r($response->error_message, TRUE) : ''));
          break;

        case 3:
          $this->logger->error('Incorrect data. Sent data is not in the correct format. ' . ((isset($response->error_message)) ? print_r($response->error_message, TRUE) : ''));
          break;

        case 5:
          $this->logger->error('Validation error. Mandatory data is missing or incorrectly filled. ' . ((isset($response->error_message)) ? print_r($response->error_message, TRUE) : ''));
          break;

        default:
          $this->logger->error('Unknown error happened. ' . ((isset($response->error_message)) ? print_r($response->error_message, TRUE) : ''));
          break;
      }

      return $response->error;
    }
  }

  /**
   * Get language-specific client of the SF API.
   *
   * @param string $lang
   *   Langcode for client.
   *
   * @return \SFAPIclient
   *   Object of SF Client.
   */
  public function getSfClient(string $lang): \SFAPIclient {
    // Load default and translated config.
    $config = $this->configFactory;
    $translated_config = ($this->languageManager instanceof ConfigurableLanguageManagerInterface) ?
      $this->languageManager->getLanguageConfigOverride($lang, 'superfaktura.settings') :
      $config;

    // Get translated values/default values if there is no config translation.
    $values = [];
    foreach (['email', 'api_key', 'company_id'] as $key) {
      if (is_null($values[$key] = $translated_config->get($key))) {
        $values[$key] = $config->get($key);
      }
    }

    $company_id = $values['company_id'];
    if (!empty($company_id)) {
      $company_id = (int) $company_id;
    }

    $api = new \SFAPIclient($values['email'], $values['api_key'], '', 'API', $company_id);
    if ($api) {

      if ($config->get('environment') === 'sandbox') {
        $api->useSandBox();
      }
    }
    else {
      $this->logger->alert('Could not create the Superfaktura object');
    }

    return $api;
  }

  /**
   * Set Client data for Superfaktura API.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @return array
   *   Client data.
   */
  public function addClient(Order $order) {
    $customer_profile = $order->getBillingProfile();
    $addresses = $customer_profile->get('address')->getValue();
    $address = reset($addresses);

    $client = [];

    if (!empty($address)) {
      $client = [
        'name' => $address['given_name'] . ' ' . $address['family_name'],
        'email' => $order->getEmail(),
        'address' => $address['address_line1'],
        'city' => $address['locality'],
        'zip' => $address['postal_code'],
        'country_iso_id' => $address['country_code'],
      ];
    }

    $extraClientData = [];

    // Set the phone based on the set field.
    $phoneFieldName = $this->configFactory->get('profile_phone_field');
    if (!empty($phoneFieldName) && $customer_profile->hasField($phoneFieldName)) {
      $phone = $customer_profile->get($phoneFieldName)->getValue();

      if (isset($phone[0]['value'])) {
        $extraClientData['phone'] = $phone[0]['value'];
      }
    }

    // Set the fax based on the set field.
    $faxFieldName = $this->configFactory->get('profile_fax_field');
    if (!empty($faxFieldName) && $customer_profile->hasField($faxFieldName)) {
      $fax = $customer_profile->get($faxFieldName)->getValue();

      if (isset($fax[0]['value'])) {
        $extraClientData['fax'] = $fax[0]['value'];
      }
    }

    // Set IČO based on the set field.
    $icoFieldName = $this->configFactory->get('profile_ico_field');
    if (!empty($icoFieldName) && $customer_profile->hasField($icoFieldName)) {
      $ico = $customer_profile->get($icoFieldName)->getValue();

      if (isset($ico[0]['value'])) {
        $extraClientData['ico'] = $ico[0]['value'];
      }
    }

    // Set DIČ based on the set field.
    $dicFieldName = $this->configFactory->get('profile_dic_field');
    if (!empty($dicFieldName) && $customer_profile->hasField($dicFieldName)) {
      $dic = $customer_profile->get($dicFieldName)->getValue();

      if (isset($dic[0]['value'])) {
        $extraClientData['dic'] = $dic[0]['value'];
      }
    }

    // Set IČ DPH based on the set field.
    $icDphFieldName = $this->configFactory->get('profile_ic_dph_field');
    if (!empty($icDphFieldName) && $customer_profile->hasField($icDphFieldName)) {
      $icDph = $customer_profile->get($icDphFieldName)->getValue();

      if (isset($icDph[0]['value'])) {
        $extraClientData['ic_dph'] = $icDph[0]['value'];
      }
    }

    $client += $extraClientData;

    // Add shipping information to client from the first Shipment's
    // shipping profile if exists.
    if (!empty($this->configFactory->get('add_shipping_data')) &&
      $order->hasField('shipments')
    ) {
      $shipments = $order->get('shipments')->referencedEntities();

      if (!empty($shipments)) {
        $shipment = reset($shipments);
        $shippingProfile = $shipment->getShippingProfile();

        if ($shippingProfile) {
          $shippingAddress = $shippingProfile->get('address')->getValue();
          $shippingAddress = reset($shippingAddress);

          if ($shippingAddress) {
            $shippingData = [
              'delivery_address' => $shippingAddress['address_line1'],
              'delivery_city' => $shippingAddress['locality'],
              'delivery_country_iso_id' => $shippingAddress['country_code'],
              'delivery_name' => $shippingAddress['given_name'] . ' ' . $shippingAddress['family_name'],
              'delivery_zip' => $shippingAddress['postal_code'],
            ];

            // Set the phone based on the set field.
            $phoneFieldName = $this->configFactory->get('profile_phone_field');
            if (!empty($phoneFieldName) && $shippingProfile->hasField($phoneFieldName)) {
              $phone = $shippingProfile->get($phoneFieldName)->getValue();

              if (isset($phone[0]['value'])) {
                $shippingData['delivery_phone'] = $phone[0]['value'];
              }
            }

            // Append shipping information.
            $client += $shippingData;
          }

        }
      }
    }

    return $client;
  }

  /**
   * Set Invoice data for Superfaktura API.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @return array
   *   Invoice data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Being thrown if invalid entity type is provided.
   */
  public function addInvoice(Order $order) {
    /** @var \Drupal\commerce_payment\Entity\Payment[] $payments */
    $payments = $this->paymentEntityManager->loadByProperties([
      'order_id' => $order->id(),
    ]);
    $alreadyPaid = FALSE;
    foreach ($payments as $payment) {
      if ($payment->getState()->value === 'completed') {
        $alreadyPaid = TRUE;
        break;
      }
    }

    $invoice = [
      'name' => $this->configFactory->get('invoice_name_prefix') . $order->getOrderNumber(),
      'variable' => sprintf("%'.08d", $order->getOrderNumber()),
      'constant' => $this->configFactory->get('constant'),
      'specific' => $this->configFactory->get('specific'),
      'already_paid' => $alreadyPaid,
      'invoice_currency' => $order->getTotalPrice()->getCurrencyCode(),
      'invoice_no_formatted' => '',
      'created' => date('Y-m-d', $order->getPlacedTime()),
      'due' => date('Y-m-d', $this->computeDueDate($order)),
      'comment' => '',
      'type' => 'regular',
    ];

    // Add shipping method if we need to submit shipping data.
    if (!empty($this->configFactory->get('add_shipping_data')) &&
      $order->hasField('shipments')
    ) {
      $shipments = $order->get('shipments')->referencedEntities();

      if (!empty($shipments)) {
        /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment **/
        $shipment = reset($shipments);
        $shippingMethod = $shipment->getShippingMethod();

        if (isset($shippingMethod) && $shippingMethod->hasField('superfaktura_delivery_type')) {
          $selectedSfDeliveryType = $shippingMethod->get('superfaktura_delivery_type')->getValue();
          $selectedSfDeliveryType = $selectedSfDeliveryType[0]['value'] ?? NULL;

          if (isset($selectedSfDeliveryType)) {
            $invoice['delivery_type'] = $selectedSfDeliveryType;
          }
        }
      }
    }

    // Add payment method.
    $paymentGatewayId = $order->get('payment_gateway')->getValue();
    $paymentGatewayId = $paymentGatewayId[0]['target_id'] ?? NULL;

    if (isset($paymentGatewayId)) {
      $paymentGatewayStorage = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway');
      $paymentGateway = $paymentGatewayStorage->load($paymentGatewayId);

      if (isset($paymentGateway)) {
        $selectedSfPaymentType = $paymentGateway->get('third_party_settings')['superfaktura']['superfaktura_payment_type'] ?? NULL;

        if (isset($selectedSfPaymentType)) {
          $invoice['payment_type'] = $selectedSfPaymentType;
        }
      }

    }

    return $invoice;
  }

  /**
   * Compute Invoice due date from config.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   Created Order.
   *
   * @return mixed
   *   Due date in seconds.
   */
  public function computeDueDate(Order $order) {
    $due_date = $order->getPlacedTime() + (3600 * 24 * $this->configFactory->get('maturity'));

    return $due_date;
  }

  /**
   * Set Order Item data for Superfaktura API.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $item
   *   Order item.
   * @param int $orderDiscount
   *   Percentage value of total Order Discount.
   * @param string $order_discount_label
   *   Label for order discount.
   *
   * @return array
   *   Order Item data.
   *
   * @todo Set 'unit' in order_item dynamically based on item type
   */
  public function addOrderItem(OrderItemInterface $item, $orderDiscount = 0, $order_discount_label = NULL) {
    $unit_price = 0;
    $tax_rate = 0;
    $itemDiscount = 0;
    $taxIncluded = FALSE;
    $item_price = Calculator::round($item->getUnitPrice()
      ->getNumber(), 4, PHP_ROUND_HALF_UP);

    $item_adjustments = $item->getAdjustments();
    foreach ($item_adjustments as $item_adjustment) {
      if ($item_adjustment->getType() == 'tax') {
        $tax_percentage = $item_adjustment->getPercentage();
        $tax_rate = Calculator::multiply($tax_percentage, 100);
        $taxIncluded = $item_adjustment->isIncluded();
      }
      elseif ($item_adjustment->getType() == 'promotion' && !$item_adjustment->isIncluded()) {
        // Add shipping discount percentage to shipping item.
        if (!is_null($item_adjustment->getPercentage())) {
          $itemDiscount = Calculator::multiply($item_adjustment->getPercentage(), 100);
          $order_item_discount_description = $item_adjustment->getLabel();
        }
        elseif (is_null($item_adjustment->getPercentage()) && !is_null($item_adjustment->getAmount()->getNumber())) {
          $item_discount_amount = Calculator::round($item_adjustment->getAmount()->getNumber(), 4, PHP_ROUND_HALF_UP);
          $full_item_price = $item_price * $item->getQuantity();
          $order_item_discount_description = $item_adjustment->getLabel();
          if (strcmp(abs($full_item_price), abs($item_discount_amount)) === 0) {
            $itemDiscount = 100;
          }
          else {
            $item_discount_percentage = Calculator::divide($item_discount_amount, $full_item_price, 4);
            $itemDiscount = abs(Calculator::multiply($item_discount_percentage, 100));
          }
        }
      }
    }

    // Calculate percentage of total discount.
    if ($orderDiscount != 0) {
      $totalDiscount = 100 - ((1 - ($itemDiscount / 100)) * (1 - ($orderDiscount / 100)) * 100);
      $order_item_discount_description = $order_discount_label;
    }
    else {
      $totalDiscount = $itemDiscount;
    }

    // Calculate and round tax amount (ta)
    // and unit price without tax from order item.
    $unit_price = $item->getUnitPrice()->getNumber();
    // If there are no tax adjustments, then we would end up without price.
    // So check if are tax rates defined.
    if ($tax_rate > 0) {
      if ($taxIncluded) {
        $ta_divisor = Calculator::add(100, $tax_rate);
        $ta_divide = Calculator::divide($unit_price, $ta_divisor);
        $ta_multiply = Calculator::multiply($ta_divide, $tax_rate);
        $tax_amount = Calculator::round($ta_multiply, 4, PHP_ROUND_HALF_UP);

        $unit_price = Calculator::subtract($unit_price, $tax_amount, 4);
      }
    }

    $order_item = [
      'name' => $item->getTitle(),
      'description' => '',
      'quantity' => $item->getQuantity(),
      'unit' => 'ks.',
      'unit_price' => $unit_price,
      'tax' => $tax_rate,
      'discount' => !empty($totalDiscount) ? $totalDiscount : 0,
      'discount_description' => $order_item_discount_description ?? NULL,
    ];

    return $order_item;
  }

  /**
   * Set Shipping data for Superfaktura API.
   *
   * @param \Drupal\commerce_order\Adjustment $shipping_adjustment
   *   Order shipping adjustment.
   * @param array $shipping_data
   *   Shipping data.
   *
   * @return array
   *   Shipping item data.
   */
  public function addShippingItem(Adjustment $shipping_adjustment, array $shipping_data) {
    $shipping_unit_price = $shipping_adjustment->getAmount()->getNumber();

    // Calculate shipping tax amount and tax rate.
    $tax_rate = Calculator::multiply($shipping_data['shipping_tax_percentage'], 100);

    if (!empty($shipping_data['shipping_tax_included_in_price'])) {
      // Shipping tax amount = (unit price / (100 + tax rate)) * tax rate.
      $ta_divisor = Calculator::add($tax_rate, '100');
      $ta_divide = Calculator::divide($shipping_unit_price, $ta_divisor);
      $shipping_tax_amount = Calculator::multiply($ta_divide, $tax_rate);
      $shipping_unit_price = Calculator::subtract($shipping_unit_price, (string) $shipping_tax_amount, 4);
    }

    $shipping_item = [
      'name' => $shipping_adjustment->getLabel(),
      'description' => '',
      'quantity' => 1,
      'unit' => 'ks.',
      'unit_price' => $shipping_unit_price,
      'tax' => $tax_rate,
    ];

    // Add shipping discount percentage to shipping item.
    if (!is_null($shipping_data['shipping_discount_percentage'])) {
      $shipping_discount_percentage = Calculator::multiply($shipping_data['shipping_discount_percentage'], 100);
      $shipping_item['discount'] = $shipping_discount_percentage;
      $shipping_item['discount_description'] = $shipping_data['shipping_discount_label'];
    }
    elseif (is_null($shipping_data['shipping_discount_percentage']) && !is_null($shipping_data['shipping_discount_amount'])) {
      $shipping_price = Calculator::round($shipping_unit_price, 4, PHP_ROUND_HALF_UP);
      $shipping_discount_amount = Calculator::round($shipping_data['shipping_discount_amount'], 4, PHP_ROUND_HALF_UP);
      $shipping_item['discount_description'] = $shipping_data['shipping_discount_label'];
      if (strcmp(abs($shipping_price), abs($shipping_discount_amount)) === 0) {
        $shipping_item['discount'] = 100;
      }
      else {
        $shipping_discount_percentage = Calculator::divide($shipping_discount_amount, $shipping_price, 4);
        $shipping_item['discount'] = abs(Calculator::multiply($shipping_discount_percentage, 100));
      }
    }

    return $shipping_item;
  }

  /**
   * Downloads the invoice file and saves it as a file entity.
   *
   * @param \SFAPIclient $api
   *   API library.
   * @param \StdClass $response
   *   Invoice object returned from SF Service.
   *
   * @return \Drupal\file\FileInterface|null
   *   The created file object. NULL if file could not be created.
   */
  public function createInvoiceFile(\SFAPIclient $api, \StdClass $response) {
    $isEveryThingSet = (isset($response) &&
      isset($response->data) &&
      isset($response->data->Invoice) &&
      isset($response->data->Invoice->id) &&
      isset($response->data->Invoice->token)
    );

    if (!$isEveryThingSet) {
      return NULL;
    }

    $invoiceId = $response->data->Invoice->id;
    $token = $response->data->Invoice->token;
    $pdfResponse = $api->getPDF($invoiceId, $token);

    if (!empty($pdfResponse) && is_object($pdfResponse) && !empty($pdfResponse->body)) {
      $fileContent = $pdfResponse->body;
      $file = NULL;

      try {
        /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
        $fileSystem = \Drupal::service('file_system');
        /** @var \Drupal\file\FileRepositoryInterface $fileRepository */
        $fileRepository = \Drupal::service('file.repository');
        $directory = 'private://sfinvoices';
        $requestTime = \Drupal::time()->getRequestTime();
        $destination = $directory . '/' . $invoiceId . '-' . $requestTime . '.pdf';

        /** @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager */
        $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager');

        // Only save data if private files are set.
        if ($stream_wrapper_manager->isValidUri($destination)) {
          $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
          $file = $fileRepository->writeData($fileContent, $destination, FileSystemInterface::EXISTS_RENAME);
        }

      }
      catch (\Exception $e) {
        $this->logger->critical($e->getMessage());
      }

      if (!$file) {
        return NULL;
      }

      return $file;
    }

    return NULL;
  }

}
