<?php

namespace Drupal\superfaktura\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SuperFaktura settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'superfaktura_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('superfaktura.settings');
    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SFAPI email'),
      '#default_value' => $config->get('email'),
      '#description' => $this->t("Enter the e-mail address you use to log in Superfaktura."),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SFAPI key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t("Enter the API key. You can obtain yours in your Superfaktura account, in Tools -> API."),
      '#required' => TRUE,
    ];

    $form['company_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company ID'),
      '#default_value' => $config->get('company_id'),
      '#description' => $this->t("Enter Company ID with which you are working through the API (if you have more comapnies)."),
    ];

    $form['invoice_name_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invoice name prefix'),
      '#default_value' => $config->get('invoice_name_prefix'),
      '#description' => $this->t("This text will be displayed as invoice name and superseded by inovice number."),
      '#required' => TRUE,
    ];

    $form['constant'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invoice constant'),
      '#default_value' => $config->get('constant'),
      '#description' => $this->t("Enter the constant identification for the issued invoices."),
      '#required' => TRUE,
    ];

    $form['specific'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invoice specific'),
      '#default_value' => $config->get('specific'),
      '#description' => $this->t("Enter the specific identification for the issued invoices."),
      '#required' => FALSE,
    ];

    $form['maturity'] = [
      '#type' => 'select',
      '#title' => $this->t('Invoice maturity'),
      '#default_value' => $config->get('maturity'),
      '#options' => [
        3 => '3 days',
        7 => '7 days',
        14 => '14 days',
        30 => '30 days',
        90 => '90 days',
      ],
      '#description' => $this->t("Enter the specific identification for the issued invoices."),
      '#required' => FALSE,
    ];

    $form['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#options' => [
        'sandbox' => $this->t('Sandbox'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $config->get('environment') ?? 'live',
    ];

    $orderFieldOptions = [
      '_none' => $this->t('- None -'),
    ];
    /** @var \Drupal\field\FieldStorageConfigStorage $fieldStorage */
    $fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config');
    $orderFields = $fieldStorage->loadByProperties([
      'entity_type' => 'commerce_order',
      'type' => 'file',
      'deleted' => FALSE,
      'status' => 1,
    ]);
    foreach ($orderFields as $orderField) {
      $orderFieldOptions[$orderField->getName()] = $orderField->getName();
    }

    $form['order_file_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Save downloaded invoice to file field'),
      '#description' => $this->t("Updating the order later doesn't update the generated invoice at Superfaktura."),
      '#options' => $orderFieldOptions,
      '#default_value' => $config->get('order_file_field') ?? '_none',
      '#empty_value' => '_none',
    ];

    $form['add_shipping_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send shipping information'),
      '#description' => $this->t("Commerce Shipping module is required to be set for this functionality."),
      '#default_value' => $config->get('add_shipping_data') ?? FALSE,
    ];

    $profileFieldOptions = [
      '_none' => $this->t('- None -'),
    ];
    /** @var \Drupal\field\FieldStorageConfigStorage $fieldStorage */
    $fieldStorage = \Drupal::entityTypeManager()->getStorage('field_storage_config');
    $profileFields = $fieldStorage->loadByProperties([
      'entity_type' => 'profile',
      'deleted' => FALSE,
      'status' => 1,
    ]);
    foreach ($profileFields as $profileField) {
      $profileFieldOptions[$profileField->getName()] = $profileField->getName();
    }

    $form['profile_ico_field'] = [
      '#type' => 'select',
      '#title' => $this->t('IČO (ID)'),
      '#options' => $profileFieldOptions,
      '#default_value' => $config->get('profile_ico_field') ?? '_none',
      '#description' => $this->t("Drupal Commerce doesn't contain a field for collecting IČO by default. To submit this field to Superfaktura, create a simple text field in Customer Profile and set that field here."),
      '#empty_value' => '_none',
    ];

    $form['profile_dic_field'] = [
      '#type' => 'select',
      '#title' => $this->t('DIČ (Tax ID)'),
      '#options' => $profileFieldOptions,
      '#default_value' => $config->get('profile_dic_field') ?? '_none',
      '#description' => $this->t("Drupal Commerce doesn't contain a field for collecting DIČ by default. To submit this field to Superfaktura, create a simple text field in Customer Profile and set that field here."),
      '#empty_value' => '_none',
    ];

    $form['profile_ic_dph_field'] = [
      '#type' => 'select',
      '#title' => $this->t('IČ DPH (VAT ID)'),
      '#options' => $profileFieldOptions,
      '#default_value' => $config->get('profile_ic_dph_field') ?? '_none',
      '#description' => $this->t("Drupal Commerce has a tax_number a field for collecting IČ DPH by default. To submit this field to Superfaktura, set it to tax_number or create a custom field in Customer Profile and use that."),
      '#empty_value' => '_none',
    ];

    $form['profile_phone_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Phone'),
      '#options' => $profileFieldOptions,
      '#default_value' => $config->get('profile_phone_field') ?? '_none',
      '#empty_value' => '_none',
      '#description' => $this->t("Drupal Commerce doesn't contain a field for collecting customer's phone number by default. To submit this field to Superfaktura, create a simple text field in Customer Profile and set that field here."),
    ];

    $form['profile_fax_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Fax'),
      '#options' => $profileFieldOptions,
      '#default_value' => $config->get('profile_fax_field') ?? '_none',
      '#empty_value' => '_none',
      '#description' => $this->t("Drupal Commerce doesn't contain a field for collecting customer's fax number by default. To submit this field to Superfaktura, create a simple text field in Customer Profile and set that field here."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('superfaktura.settings')
      ->set('email', $form_state->getValue('email'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('company_id', $form_state->getValue('company_id'))
      ->set('invoice_name_prefix', $form_state->getValue('invoice_name_prefix'))
      ->set('constant', $form_state->getValue('constant'))
      ->set('specific', $form_state->getValue('specific'))
      ->set('maturity', $form_state->getValue('maturity'))
      ->set('environment', $form_state->getValue('environment'))
      ->set('order_file_field', $form_state->getValue('order_file_field'))
      ->set('add_shipping_data', $form_state->getValue('add_shipping_data'))
      ->set('profile_ico_field', $form_state->getValue('profile_ico_field'))
      ->set('profile_dic_field', $form_state->getValue('profile_dic_field'))
      ->set('profile_ic_dph_field', $form_state->getValue('profile_ic_dph_field'))
      ->set('profile_phone_field', $form_state->getValue('profile_phone_field'))
      ->set('profile_fax_field', $form_state->getValue('profile_fax_field'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['superfaktura.settings'];
  }

}
