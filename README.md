CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers
* Sponsors


INTRODUCTION
------------

SuperFaktura is an easy to use online app, that allows you to create
great looking online invoices, proformas, price estimates, orders,
delivery and credit notes. You can invoice in foreign currencies and languages.
Accept payments online, organize your stock management,
send electronic invoices online, or in paper form via hybrid post.

REQUIREMENTS
------------

- you need SuperFaktura API (SFAPI)
https://github.com/superfaktura/apiclient (if you install this Drupal module via
composer - SFAPI is installed as dependency)
- in order to support tax on shipping apply patch to Drupal Commerce
https://www.drupal.org/project/commerce_shipping/issues/2874158

INSTALLATION
------------

- with composer

    ```
     composer require drupal/superfaktura
    ```

- with drush

    ```
     drush en superfaktura
    ```

CONFIGURATION
--------------

After installation you need to set your SFAPI email and key (from your
SuperFaktura account) and other invoice details on "Superfaktura settings" page

```
/admin/commerce/config/superfaktura
```

If you want to download the generated invoice PDF to an order field when it's created (updating the order later doesn't update the generated invoice at Superfaktura):
- Set Drupal to use private files because you probably don't want others to access all the generated PDF invoices. Follow https://www.drupal.org/docs/8/core/modules/file/overview#private-file-system and clear the caches with the drush cr command. Without the private files set correctly, the PDF save will not work.
- Go to Order Type fields (default is `/admin/commerce/config/order-types/default/edit/fields` )
- Create a File field. Set it to use private file system.
- Refresh the "Superfaktura settings" page and set the newly created file field at the "Save downloaded invoice to file field". Click save.

For each payment gateway (`/admin/commerce/config/payment-gateways`) and shipping method (`/admin/commerce/shipping-methods`) by editing one you can set what category they belong using the "Superfaktura payment type" and "Superfaktura delivery type" fields.

Next step is to set when should the invoice creating service run. If you want to send invoices just when the order was placed, then enable the **superfaktura_invoice_place** submodule.

Otherwise create an own module with subscriber to call SuperFaktura service.
In example below SuperFaktura service is called when Order is placed.

```
<?php

namespace Drupal\your_module\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Superfaktura event subscriber.
 */
class SuperfakturaSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.pre_transition' => ['createInvoice', -200],
    ];
  }

  /**
   * Create Invoice in Superfaktura from created order.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Transition Event.
   */
  public function createInvoice(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $event->getEntity();
    $invoice = \Drupal::service('superfaktura.invoice_service');
    $invoice->createInvoice($order);
  }

}
```


MODULE EVENTS
-----------
You can alter the module's behavior with custom code using EventSubscribers.

For available events see `superfaktura/src/Event/InvoiceEvents.php`.


MAINTAINERS
-----------

- Miro Michalicka (D8 branch), https://www.drupal.org/u/mirom
- Peter Hrabovcin (D8 branch), https://www.drupal.org/u/pedro_sv
- Peter Lachky (D7 branch), https://www.drupal.org/u/petiar

SPONSORS
--------

The development of this module is sponsored by
[SuperFaktura](https://www.superfaktura.sk/ "SuperFaktura")
