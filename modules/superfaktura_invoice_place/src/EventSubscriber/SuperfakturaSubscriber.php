<?php

namespace Drupal\superfaktura_invoice_place\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

/**
 * Superfaktura event subscriber.
 */
class SuperfakturaSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.pre_transition' => ['createInvoice', -200],
    ];
  }

  /**
   * Create Invoice in Superfaktura from created order.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   Transition Event.
   */
  public function createInvoice(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $event->getEntity();
    /** @var \Drupal\superfaktura\InvoiceService $invoice */
    $invoice = \Drupal::service('superfaktura.invoice_service');
    $invoice->createInvoice($order);
  }

}
